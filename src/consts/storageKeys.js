export const STORAGE_KEY_CONFIG = "game-config";
export const STORAGE_KEY_RESULT = "game-result";
export const STORAGE_KEY_QUESTIONS = "game-questions";
export const STORAGE_KEY_ANSWERS = "all-answers";