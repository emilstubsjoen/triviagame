export function getCategoryNames() {
  return fetch("https://opentdb.com/api_category.php")
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Could not fetch categories 😭");
      }

      return response.json();
    })
    .then((data) => data.trivia_categories);
}

export function getQuestions(numOfQuestions, category, difficulty, type) {
  const fetchURL = getURL(numOfQuestions, category, difficulty, type);
  return fetch(fetchURL)
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Could not fetch questions 😭");
      }
      return response.json();
    })
    .then((data) => data.results);
}

function getURL(numOfQuestions, category, difficulty, type) {
  let URL = `https://opentdb.com/api.php?amount=${numOfQuestions}`;
  if (category !== "any" || difficulty !== "any" || type !== "any") {
    URL += "&";
  }

  if (category != "any") {
    URL += `category=${category.id}`;
    if (difficulty != "any" || type != "any") {
      URL += "&";
    }
  }

  if (difficulty != "any") {
    URL += `difficulty=${difficulty}`;
    if (type != "any") {
      URL += "&";
    }
  }

  if (type != "any") {
    URL += `type=${type.type}`;
  }

  return URL;
}
