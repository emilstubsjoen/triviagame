import Vue from "vue";
import VueRouter from "vue-router";
import StartScreen from "@/components/StartScreen/StartScreen.vue";
import QuestionScreen from "@/components/QuestionScreen/QuestionScreen.vue";
import ResultScreen from "@/components/ResultScreen/ResultScreen.vue";

Vue.use(VueRouter); // Add the Router features to the Vue Object

const routes = [
  {
    path: "/start",
    alias: "/",
    component: StartScreen,
  },
  {
    path: "/question",
    component: QuestionScreen,
  },
  {
    path: "/result",
    component: ResultScreen,
  },
];

export default new VueRouter({ routes });
